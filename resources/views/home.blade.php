@section('title')
    {{--User Dashboard--}}
    {{ Auth::user()->name }}
@endsection
@extends('frontEnd.master')
@section('mainContent')
    <div id="page-wrapper" class="sign-in-wrapper">
        <div class="graphs">
            <h2 class="text-center text-success">{{Session::get('message')}}</h2>
            You are logged in as user
            <div>
                <a href="{{'/sell'}}">Sell</a>
            </div>
        </div>
    </div>
@endsection
{{--@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as User!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection--}}
